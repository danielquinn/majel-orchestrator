#!/usr/bin/env bash

set -e

export MAKEFLAGS="-j$(getconf _NPROCESSORS_ONLN)"

if [ "${UID}" != "0" ]; then
  echo "Only root can run this thing"
  exit 1
fi


# Install Dependencies

# Basic system stuff
apt-get install -y \
  xorg \
  gnome \
  gnome-software \
  firefox-esr \
  kodi \
  scrot \
  python3-pip \
  python3-venv

# Chromium
apt-get install -y \
  chromium-browser \
  rpi-chromium-mods \
  libwidevinecdm0

# PyAutoGUI
apt-get install -y \
  python3-tk \
  python3-dev

# Various bits for Python libraries
apt-get install -y \
  libcairo2-dev \
  libgirepository1.0-dev \
  gobject-introspection

# Audio
apt-get install -y \
  portaudio19-dev

# Pillow
apt-get install -y \
  libjpeg62-turbo-dev \
  libpng-dev \
  libgif-dev \
  librsvg2-dev


# Build Kodi from Source to avoid annoying Rpi-only defaults

apt install -y \
  debhelper \
  autoconf \
  automake \
  autopoint \
  gettext \
  autotools-dev \
  cmake \
  curl \
  default-jre \
  doxygen \
  gawk \
  gcc \
  gdc \
  gperf \
  libasound2-dev \
  libass-dev \
  libavahi-client-dev \
  libavahi-common-dev \
  libbluetooth-dev \
  libbluray-dev \
  libbz2-dev \
  libcdio-dev \
  libp8-platform-dev \
  libcrossguid-dev \
  libcurl4-openssl-dev \
  libcwiid-dev \
  libdbus-1-dev \
  libdrm-dev \
  libegl1-mesa-dev \
  libenca-dev \
  libflac-dev \
  libflatbuffers-dev \
  libfmt-dev \
  libfontconfig-dev \
  libfreetype6-dev \
  libfribidi-dev \
  libfstrcmp-dev \
  libgcrypt20-dev \
  libgif-dev \
  libgles2-mesa-dev \
  libgl1-mesa-dev \
  libglu1-mesa-dev \
  libgnutls28-dev \
  libgpg-error-dev \
  libgtest-dev \
  libiso9660-dev \
  libjpeg-dev \
  liblcms2-dev \
  libltdl-dev \
  liblzo2-dev \
  libmicrohttpd-dev \
  libmariadb-dev \
  libnfs-dev \
  libogg-dev \
  libpcre3-dev \
  libplist-dev \
  libpng-dev \
  libpulse-dev \
  libshairplay-dev \
  libsmbclient-dev \
  libspdlog-dev \
  libsqlite3-dev \
  libssl-dev \
  libtag1-dev \
  libtiff5-dev \
  libtinyxml-dev \
  libtool \
  libudev-dev \
  libunistring-dev \
  libva-dev \
  libvdpau-dev \
  libvorbis-dev \
  libxmu-dev \
  libxrandr-dev \
  libxslt1-dev \
  libxt-dev \
  lsb-release \
  meson \
  nasm \
  ninja-build \
  python3-dev \
  python3-pil \
  python3-pip \
  rapidjson-dev \
  swig \
  unzip \
  uuid-dev \
  zip \
  zlib1g-dev

cd ${HOME}
git clone https://github.com/xbmc/xbmc kodi
mkdir kodi-build
cd kodi-build
cmake ../kodi \
  -DCMAKE_INSTALL_PREFIX=/usr/local \
  -DCORE_PLATFORM_NAME=x11 \
  -DAPP_RENDER_SYSTEM=gl
make install
cd ${HOME}
rm -rf kodi-build kodi


# Force GNOME to start with Xorg instead of Wayland

sed -i -E 's/^#WaylandEnable/WaylandEnable/' /etc/gdm3/daemon.conf


# Install Majel into a virtualenv at /opt/majel

python -m venv /opt/majel
. /opt/majel/bin/activate
pip install majel
deactivate


# Add a startup script so the user can type `<Super>+Majel+<Enter>`

cat <<EOF > /usr/share/applications/majel.desktop
[Desktop Entry]
Version=1.0
Name=Majel
Comment=Automate your desktop with your voice
Exec=gnome-terminal -e "bash -c '/opt/majel/bin/majel;\$SHELL'"
Icon=utilities-terminal
Terminal=false
Type=Application
Categories=Application;
EOF


# When a user starts an X session, configure the key commands required to get
# Majel running and then create a file in ${HOME}/.config/majel to make sure
# that this only runs once.

cat <<EOF > /usr/local/bin/majel-setup
#!/usr/bin/env bash

if [ -e "${HOME}/.config/majel/autostart-has-run" ]; then
  exit 0
fi

# Keybindings
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/']"

# Keybinding: Listen
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Primary><Shift>F11"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "/opt/majel/bin/majel-controller start"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Majel Start Listening"

# Keybinding: Stop
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Primary><Shift>F12"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "/opt/majel/bin/majel-controller stop"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "Majel Stop Listening"

# Keybindings: Navigation
gsettings set org.gnome.shell.keybindings toggle-overview "['<Super>s']"
gsettings set org.gnome.desktop.wm.keybindings maximize "['<Super>Page_Up']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Primary>F1']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Primary>F2']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Primary>F3']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Primary>F4']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-5 "['<Primary>F5']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-6 "['<Primary>F6']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-7 "['<Primary>F7']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-8 "['<Primary>F8']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-9 "['<Primary>F9']"
gsettings set org.gnome.mutter dynamic-workspaces false
gsettings set org.gnome.desktop.wm.preferences num-workspaces 9

mkdir -p \${HOME}/.config/majel
touch \${HOME}/.config/majel/autostart-has-run
EOF

chmod 755 /usr/local/bin/majel-setup


# We assume that the primary user on this pi has id 1000

cat <<EOF > /etc/xdg/autostart/majel-setup.desktop
[Desktop Entry]
Type=Application
Name=Majel Setup
Exec=/usr/local/bin/majel-setup
NoDisplay=true
EOF


# With the installation of GNOME and NetworkManager, dhcpcd is now redundant
# and may get in the way of things.

systemctl disable dhcpcd


# Rename the pi user

usermod -l majel pi
usermod -d /home/majel -m majel


# Add majel to a few important groups
usermod -a -G input,video,bluetooth majel


# We're all set.  Reboot to (hopefully) have everything working properly.

reboot
