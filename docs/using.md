# Using Majel

Once installed & configured, you should be able to just run `majel` on the
command line and watch while browsers are opened on various workspaces.  When
it's all ready to go, and if you've got Mimic installed, you should hear a
little voice say *"Majel is ready"*.

Great!  But how do I actually *use* this thing?


## Triggering the Listener

Unlike other voice-driven assistants, Majel does *not* make use of a "hot 
word".  There won't be a case of you saying "Alexa... ALEXA" to try and get
Majel's attention.  Instead you just call a command: `majel-controller` and
pass to it whatever you like.  For example:

```shell
$ majel-controller listen
```

...will trigger the listener.  You'll hear a little chirp from your speakers,
after which you can say whatever command you want, like:

> *"Open nautilus"*

Majel will then do as you ask.  There's a variety of different
[handlers](architecture.md#handlers) for your various commands, including one
that handles [custom commands](extending.md#custom-commands).  Here's a
reasonably thorough list of examples for you to work from:

| Command                               | Description                                                                                                                                                                                                                                                                                    |
|---------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| *"Open [program]"*                    | Searches your local list of applications and if it finds an application with that name, opens it on the current workspace.                                                                                                                                                                     |
| *"Watch The Expanse on Kodi"*         | Queries Kodi to confirm that you do indeed have The Expanse locally, and if yes, then switches to the Kodi workspace to play the next episode.                                                                                                                                                 |
| *"Watch The Witcher on Netflix"*      | Checks if your system supports streaming, if yes, asks Utelly for the URL where Netflix shows The Witcher, switches to the workspace where Netflix is waiting, goes to said URL and clicks "Play".                                                                                             |
| *"Watch Star Trek Picard on Netflix"* | Checks if your system supports streaming, if yes, asks Utelly for the URL where Amazon Prime Video shows Star Trek: Picard, switches to the workspace where Prime is waiting, goes to said URL and clicks "Play".                                                                              |
| *"Play the Witcher"*                  | Checks if your system supports streaming, if yes, asks Utelly for a list of who carries The Witcher while simultaneously querying Kodi (if you've enabled it) to see if you've got it there.  It then switches to the appropriate workspace, finds the show, and starts it where you left off. |
| *"YouTube Baby Shark"*                | Searches Google's API for `Baby Shark`, gets a URL for it, switches to the workspace where YouTube is sitting and then goes to the URL in question using `yout-ube.com`, a handy service that automatically shows any YouTube video in full screen, running in a loop.                         |
| *"Say [whatever you like]"*           | Just has Majel respond with whatever you asked it to say.  Note that you must have an TTS engine like Mimic installed for this to work.                                                                                                                                                        |
| *"Stop"*                              | Whatever Majel is doing right now, it should stop that.  Typically this means stopping playing of a video.                                                                                                                                                                                     |


## Programmatically Stopping

Say things are out of hand, or the room is just too noisy for Majel to be able
to hear you say "stop".  You can tell it to stop via a command:

```shell
$ majel-controller stop
```


## Programmatically Issuing a Command

The controller script will also allow you to send an arbitrary command to
Majel:

```shell
$ majel-controller 'open nautilus'
```

This is mostly handy for development as it's a little weird to be sitting in
your chair all alone at 3am saying "open nautilus" repeatedly until it works
:-)


## This is Stupid.  I Don't Want to Have to Type Stuff Every Time

Yeah that's not a very good interface out-of-the-box, which is why I recommend
mapping the execution of these commands to keyboard shortcuts.  For example,
you can run the following commands to map the `listen` and `stop` commands to
`<Ctrl><Shift><F11>` and `<Ctrl><Shift><F12>` respectively:

```shell
# Keybinding: Listen
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Primary><Shift>F11"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "/opt/majel/bin/majel-controller listen"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Majel Start Listening"

# Keybinding: Stop
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Primary><Shift>F12"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "/opt/majel/bin/majel-controller stop"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "Majel Stop Listening"
```

You can also set these keymappings via the UI by going to `Settings` >
`Keyboard` > `View and Customise Shortcuts` > `Custom Shortcuts` and using the
UI there to create your mappings.


### What Am I Supposed to do, Have a Keyboard in my Living Room?

Not necessarily.  You can pick up [one of these Flic buttons](https://flic.io/shop/flic-2-single-pack)
and configure it to emit `<Ctrl><Shift><F11>` on click and `<Ctrl><Shift><F12>`
on double-click.  They're low-power Bluetooth devices with a crazy-long battery
life.  They play well with Linux too :-)


### No, I Want Support for Hot Words

Sure ok, that's cool.  Majel doesn't do that yet.  I might add support for it,
but as I prefer offline solutions (rather than streaming all the audio in my
house to Google), supporting hot words may be tricky.  If you have ideas around
how to do this, by all means send a merge request.
