from pathlib import Path
from time import sleep
from typing import Dict

import pyautogui
import yaml

from ...helpers.gui import gui
from ..base import Handler


class UserHandler(Handler):

    PRIORITY = 0

    COMMAND_DIR = Path.home() / ".config" / "majel" / "commands"

    def __init__(self, manager):
        super().__init__(manager)
        self.__commands = self.collect_commands()

    def __str__(self):
        return "User"

    def can_handle(self, command: str):
        return command in self.__commands

    def handle(self, command: str) -> None:

        cmd = self.__commands[command]
        for step in cmd["steps"]:

            type_ = step["type"]
            argument = step["argument"]

            if type_ == "command":
                self._handle_command(argument)
            elif type_ == "click":
                self._handle_click(cmd["path"], argument)
            elif type_ == "type":
                self._handle_type(argument)
            elif type_ == "wait":
                sleep(argument)
            else:
                self.logger.error("Received an invalid step type: %s", type_)

    def _handle_command(self, argument: str) -> None:
        """
        Templated commands can themselves call arbitrary commands, letting you
        get recursive if you wanna go there. :-)
        """
        self.logger.debug("Executing subcommand: %s", argument)
        self.manager.execute(argument)

    def _handle_click(self, path: Path, argument: str) -> None:
        screenshot = path / argument
        self.logger.debug("Clicking: %s", screenshot)
        gui.click_image(screenshot)

    def _handle_type(self, argument: str) -> None:
        self.logger.debug("Typing: %s followed by the [enter] key", argument)
        pyautogui.typewrite(list(argument) + ["enter"])

    def collect_commands(self) -> Dict[str, dict]:
        """
        User-generated commands live outside the codebase in their own home.
        This is where we pull in those commands so that we can register them
        for use.
        """

        r = {}
        for folder in self.COMMAND_DIR.glob("*"):

            command_config = folder / "command.yaml"
            if not command_config.exists():
                self.logger.warning(
                    'Command "%s" does not have a "command.yaml" file.  '
                    "Skipping.",
                    folder,
                )
                continue

            self.logger.info("Ingesting command: %s", folder)

            with command_config.open() as c:
                cmd = yaml.safe_load(c)
            r[cmd["command"]] = {"path": folder, "steps": cmd["steps"]}

        return r
