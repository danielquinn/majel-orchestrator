from ...voices.base import voice
from ..base import Handler


class SayHandler(Handler):

    PRIORITY = 0.2

    def __str__(self):
        return "Say"

    def can_handle(self, command: str) -> bool:
        return command.startswith("say ")

    def handle(self, command: str) -> None:
        voice.tts(command[4:])
