from ...audio.player import notifications
from ...config import config
from ...voices.base import voice
from ..base import Handler


class UnhandledHandler(Handler):

    PRIORITY = 0.9

    CONFIG = config.handlers.unhandled

    def __str__(self):
        return "Unhandled"

    @classmethod
    def can_handle(cls, command: str) -> bool:
        return True

    def handle(self, command: str) -> None:
        notifications.error.play()
        if self.CONFIG.debug_stt_with_tts and command:
            voice.tts(f"I heard: {command}")
