from .....config import HandlerConfig


class NetflixConfig(HandlerConfig):
    profile: str = "Netflix"
    workspace: int = 2
    is_enabled: bool = False
