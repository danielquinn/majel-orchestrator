import re

from .....config import config
from ..streamer import StreamerHandler


class NetflixHandler(StreamerHandler):

    PRIORITY = 0.5

    CONFIG = config.handlers.netflix
    REQUIRED_CONFIG = ("profile", "is_enabled", "profile")

    WORKSPACE = CONFIG.workspace
    PROFILE_NAME = config.handlers.netflix.profile
    START_URL = "https://netflix.com/browse"

    WATCH_REGEX = re.compile(r"^watch (?P<title>.*?) on (netflix|net flicks)$")
    URL_REGEX = re.compile(r"^netflix:(?P<url>http.*)$")

    @staticmethod
    def process_url(url: str) -> str:
        """
        Sometimes, the Utelly API will return the Netflix title page rather
        than the watch page.  This is our work-around.
        """
        return url.replace("/title/", "/watch/")
