import re

from pathlib import Path
from typing import Optional

from ....exceptions import CantHandleError
from ....helpers.gui import Region, gui
from ....helpers.utelly import utelly
from ..base import ChromiumHandler


class StreamerHandler(ChromiumHandler):

    # The regex we use when the user says something like
    # "watch <title> on <service>"
    WATCH_REGEX: re.Pattern

    # The regex we use for internal handling, typically the result of an API
    # call to Utelly.  These normally look something like netflix:https://...
    URL_REGEX: re.Pattern

    # The image to look for that tells us we're ok to press play
    READY_IMAGE: Optional[Path] = None
    READY_REGION: Optional[Region] = None

    def can_handle(self, command: str) -> bool:
        if self.URL_REGEX.match(command):
            return True
        if self.WATCH_REGEX.match(command):
            if utelly.is_enabled:
                return True
        return False

    def handle(self, command: str) -> None:

        self.logger.info("Attempting to handle %s", command)

        if m := self.URL_REGEX.match(command):
            self.switch_to_workspace()
            self.go_to(self.process_url(m.group("url")))
            self.press_play()
            return

        if m := self.WATCH_REGEX.match(command):
            if result := utelly.search(m.group("title")):
                self.switch_to_workspace()
                self.go_to(self.process_url(result[1]))
                self.press_play()
                return

        raise CantHandleError()

    def press_play(self) -> None:
        """
        Move the mouse to the centre of the screen and click.
        """
        self.logger.info("Playing")
        gui.click(gui.centre, duration=0.3)
        self.is_playing = True

    @staticmethod
    def process_url(url: str) -> str:
        return url
