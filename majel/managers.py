from typing import List

from .exceptions import CantHandleError
from .handlers.base import Handler
from .logger import Loggable


class Manager(Loggable):
    """
    This keeps a record of all the handlers in play and is able to find the
    right one for the job.
    """

    def __init__(self):
        self.library = ()

    def start(self):
        self.logger.info("Setting up workspaces (this can take a while)")
        for handler in self._get_sorted_leaves():
            instance = handler(self)
            if instance.is_enabled:
                self.logger.info("Initialising %s", handler.__name__)
                self.library = self.library + (instance,)

    def execute(self, command: str) -> None:
        for handler in self.library:
            self.logger.info("Checking if %s can handle this", handler)
            if handler.can_handle(command):
                self.logger.info("Attempting to handle this with %s", handler)
                try:
                    handler.handle(command)
                    break
                except CantHandleError as e:
                    self.logger.warning(
                        'The handler "%s" claimed that it could handle "%s", '
                        "but it turns out that it couldn't: %s",
                        handler,
                        command,
                        e,
                    )

    def stop(self) -> None:
        """
        The global stop trigger.  This tells all handlers to stop what they're
        doing, though those handlers can make the decision individually as to
        whether they care.  For example, a web browser playing a YouTube video
        should probably stop playing, but a browser showing a recipe should
        ignore this message.
        """
        for handler in self.library:
            self.logger.info("Telling the %s handler to stop", handler)
            handler.stop()

    def shutdown(self):
        for handler in self.library:
            self.logger.info("Shutting down the %s handler", handler)
            handler.shutdown()

    @staticmethod
    def _get_sorted_leaves() -> List[Handler]:
        return sorted(Handler.get_subclass_leaves(), key=lambda _: _.PRIORITY)
