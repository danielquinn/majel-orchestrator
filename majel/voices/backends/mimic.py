from shutil import which
from subprocess import run

from .base import VoiceBackend


class MimicVoiceBackend(VoiceBackend):
    """
    This is the voice used for the Mycroft Project (https://mycroft.ai/),
    though we're using the "Classic" Mimic here, since Mimic 2 is a pain to
    install.

    The benefit of using Mimic is that everything is done offline, so you're
    not sending anything to a third party and the turn-around time on getting
    a voice is really quick.

    The downside is that it's not as pretty-sounding as what you might get from
    Google, Amazon, or Micros~1.
    """

    def __init__(self):

        super().__init__()

        self.binary = which("mimic")
        if not self.binary:
            self.logger.warning(
                "The `mimic` program does not appear to be installed, so it "
                "can't be used for voice synthesis."
            )
            return

        self.tts_is_available = True

    def tts(self, text: str) -> None:

        self.logger.info("Doing TTS with Mimic")

        run([self.binary, "-t", text])
