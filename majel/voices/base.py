from dataclasses import dataclass
from pathlib import Path
from random import choice
from typing import List

from ..logger import Loggable
from .backends.base import VoiceBackend


@dataclass
class Backends:

    tts: List[VoiceBackend]
    stt: List[VoiceBackend]

    @property
    def tts_names(self) -> str:
        return self._names("tts")

    @property
    def stt_names(self) -> str:
        return self._names("stt")

    def _names(self, group: str) -> str:
        return ", ".join(str(_) for _ in getattr(self, group))


class Voice(Loggable):
    def __init__(self) -> None:

        self.backends = Backends(tts=[], stt=[])
        self._setup_backends()

    def _setup_backends(self) -> None:

        for klass in VoiceBackend.__subclasses__():
            self.logger.debug(
                "Checking %s for stt/tts availability.", klass.__name__
            )
            backend = klass()
            if backend.tts_is_available:
                self.logger.debug("  TTS is available")
                self.backends.tts.append(backend)
            if backend.stt_is_available:
                self.logger.debug("  STT is available")
                self.backends.stt.append(backend)

        if self.backends.tts:
            self.logger.info(
                "TTS backends available: %s", self.backends.tts_names
            )
        else:
            self.logger.warning(
                "No TTS backend detected.  Majel cannot speak."
            )

        if self.backends.stt:
            self.logger.info(
                "STT backends available: %s", self.backends.stt_names
            )
        else:
            self.logger.warning(
                "No STT backend detected.  Majel cannot hear you."
            )

    def tts(self, text: str) -> None:

        self.logger.debug("TTS in action: %s", text)
        if not self.backends.tts:
            self.logger.warning("TTS is unavailable")
            return

        backend = choice(self.backends.tts)

        self.logger.debug("TTS Backend selected: %s", backend)

        backend.tts(text)

    def stt(self, audio: Path) -> str:

        if not self.backends.stt:
            self.logger.warning("STT is unavailable")
            return ""

        backend = choice(self.backends.stt)

        self.logger.debug("STT Backend selected: %s", backend)

        return backend.stt(audio)


voice = Voice()
