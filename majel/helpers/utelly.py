import json

from hashlib import md5
from pathlib import Path
from typing import Dict, Optional, Sequence, Tuple

import requests

from ..config import config
from ..logger import Loggable


Locations = Sequence[Dict[str, str]]


class Utelly(Loggable):

    CACHE = Path.home() / ".cache" / "majel" / "utelly"
    HOST = "utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com"
    ENDPOINT = f"https://{HOST}/lookup"

    SOURCE_NETFLIX = "netflix"
    SOURCE_PRIME = "prime"

    def __init__(self):

        self.api_key = config.apis.utelly.key
        self.country = config.apis.utelly.country
        self.streaming_service_order = config.apis.utelly.service_order

        self.CACHE.mkdir(exist_ok=True, parents=True)

        if all((self.api_key, self.country, config.apis.utelly.is_enabled)):
            self.is_enabled = True
            return

        self.is_enabled = False
        self.logger.warning(
            "Utelly isn't configured.  "
            "Streaming service detection is unavailable.\n"
            "== Current Configuration ======================\n"
            "  api_key: %s\n"
            "  country: %s\n"
            "  service_order: %s\n"
            "  is_enabled: %s\n",
            self.api_key,
            self.country,
            self.streaming_service_order,
            config.apis.utelly.is_enabled,
        )

    def search(
        self,
        query: str,
        service_order: Optional[Sequence[str]] = None,
    ) -> Optional[Tuple[str, str]]:
        """
        Hit up the Utelly API to see what they've got for `query`.  Return a
        URL for the first service in `service_order` that has the show you're
        asking about.

        The default value for `service_order` is ("netflix", "prime") or
        whatever you've configured in your config.yaml under
        Utelly > service_order

        Example:
            >>> utelly.search("The Expanse", ("netflix", "prime"))
            (
                'prime',
                'https://watch.amazon.co.uk/watch?gti=amzn1.dv.gti.28bb10a4-64a9-8d0c-df47-9d0b357e549b&tag=utellycom00-21'
            )
        """

        return self._get_location_by_availability(
            self._get_locations(query),
            service_order=service_order or config.apis.utelly.service_order,
        )

    def _get_locations(self, query: str) -> Locations:

        # TODO: Add ctime checking for cache invalidation
        cache_file = self.CACHE / md5(query.encode()).hexdigest()
        if cache_file.exists():
            with cache_file.open() as f:
                self.logger.info(f'Cache hit for "{query}"')
                return json.load(f)

        self.logger.info(
            'Sending request to Utelly to see who\'s got "%s"', query
        )
        response = requests.get(
            self.ENDPOINT,
            headers={
                "x-rapidapi-host": self.HOST,
                "x-rapidapi-key": self.api_key,
            },
            params={"term": query, "country": self.country},
        ).json()

        self.logger.debug("Utelly response received: %s", response)

        try:
            locations = response["results"][0]["locations"]
        except (KeyError, IndexError):
            self.logger.error(
                "Utelly responded with something we couldn't understand: %s",
                response,
            )
            return ()

        with cache_file.open("w") as f:
            f.write(json.dumps(locations))

        return locations

    def _get_location_by_availability(
        self,
        locations: Locations,
        service_order: Sequence[str],
    ) -> Optional[Tuple[str, str]]:
        """
        Given a list of locations from the API, look through it to see if those
        locations match any of our supported video services and return the
        first one that matches.
        """

        names = {
            self.SOURCE_NETFLIX: "Netflix",
            self.SOURCE_PRIME: "Amazon Prime Video",
        }

        for service in service_order:
            for location in locations:
                if location["display_name"] == names[service]:
                    return service, location["url"]

        return None


utelly = Utelly()
