from pathlib import Path
from random import choice
from typing import Tuple

from playsound import playsound

from ..logger import Loggable


class Playable(Loggable):
    def __init__(self, path: Path):
        self.path = path

    def play(self):
        self.logger.info("Playing: %s", self.path)
        playsound(self.path)


class Notifications:

    DEFAULT = Path(__file__).parent / "data"
    OVERRIDE_PATH = Path.home() / ".config" / "majel" / "notifications"

    @property
    def acknowledged(self) -> Playable:
        if overrides := self._get_overrides("acknowledged"):
            return Playable(choice(overrides))
        return Playable(self.DEFAULT / "acknowledged.wav")

    @property
    def error(self) -> Playable:
        if overrides := self._get_overrides("error"):
            return Playable(choice(overrides))
        return Playable(self.DEFAULT / "error.wav")

    def _get_overrides(self, name: str) -> Tuple:
        return tuple((self.OVERRIDE_PATH / name).glob("*.wav"))


notifications = Notifications()
